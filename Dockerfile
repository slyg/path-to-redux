FROM node:7.10.0-alpine

COPY package.json yarn.lock .babelrc ./

RUN yarn
