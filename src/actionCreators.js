import { INCREMENT, DECREMENT, SWITCH_THEME, UPDATE_COMMENT, ADD, SET_NUMBER_OF_COUNTERS } from './actionTypes'

export const decrement = index => ({
  type: DECREMENT,
  index
})

export const increment = index => ({
  type: INCREMENT,
  index
})

export const switchTheme = () => ({
  type: SWITCH_THEME
})

export const updateComment = value => ({
  type: UPDATE_COMMENT,
  value
})

export const add = () => ({
  type: ADD
})

export const setNumberOfCounters = num => ({
  type: SET_NUMBER_OF_COUNTERS,
  num: +num
})
