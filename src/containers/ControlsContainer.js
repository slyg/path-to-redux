import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { add } from '../actionCreators'

import Controls from '../components/Controls'

const mapDispatchToProps = dispatch => bindActionCreators({
  onAdd: add
}, dispatch)

export default connect(undefined, mapDispatchToProps)(Controls)