import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { decrement, increment } from '../actionCreators'

import CounterList from '../components/CounterList'

const mapStateToProps = state => ({
  counts: state.counters,
  isDark: state.theme === 'dark'
})

const mapDispatchToProps = dispatch => bindActionCreators({
  onIncrement: increment,
  onDecrement: decrement
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(CounterList)