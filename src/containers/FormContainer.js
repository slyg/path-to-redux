import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import MyForm from '../components/MyForm'

import { setNumberOfCounters } from '../actionCreators'

const mapDispatchToProps = dispatch => bindActionCreators({
  onSubmit: setNumberOfCounters
}, dispatch)

export default connect(undefined, mapDispatchToProps)(MyForm)
