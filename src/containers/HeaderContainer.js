import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { switchTheme } from '../actionCreators'
import Header from '../components/Header'

const mapDispatchToProps = dispatch => bindActionCreators({
  onToggle: switchTheme
}, dispatch)

export default connect(undefined, mapDispatchToProps)(Header)