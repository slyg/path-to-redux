import { connect } from 'react-redux'

import Comment from '../components/Comment'

const mapStateToProps = state => ({
  text: state.comment,
})

export default connect(mapStateToProps)(Comment)