import React from 'react'


const DARK = '#000'
const LIGHT = '#fff'

const Count = ({value, isDark}) =>
  <div style={{
    padding: 20,
    backgroundColor: isDark ? DARK : LIGHT,
    color: isDark ? LIGHT : DARK,
    width: 50,
    margin: '0 auto',
  }}>{value}</div>
  
// const OppositeCount = Comp => props => {
//   const newProps = {...props, value: (-1 * props.value)}
//   return <Comp {...newProps} />
// }
// 
// const SquareCount = Comp => props => {
//   const newProps = {...props, value: (props.value * props.value) }
//   return <Comp {...newProps} />
// }
// 
// Component composition example
// 
// export default SquareCount(Count)
  
export default Count