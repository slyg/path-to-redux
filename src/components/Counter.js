import React from 'react'

import Decrement from './Decrement'
import Count from './Count'
import Increment from './Increment'

const Counter = ({value, onIncrement, onDecrement, isDark}) =>
  <div>
    <Decrement onClick={onDecrement} />
    <Count value={value} isDark={isDark} />
    <Increment onClick={onIncrement} />
  </div>
  

export default Counter