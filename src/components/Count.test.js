import React from 'react'
import { shallow } from 'enzyme'
import Count from './Count'

describe('<Count />', () => {
  
  it('should render given value', () => {
    const wrapper = shallow(<Count value={10} />)
    expect(wrapper.find('div').text()).toMatchSnapshot()
  })
  
})