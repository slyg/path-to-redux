import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'

const Increment = ({onClick}) =>
  <RaisedButton onTouchTap={onClick}>+</RaisedButton>
  
export default Increment