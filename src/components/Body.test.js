import React from 'react'
import { shallow } from 'enzyme'
import Body from './Body'

describe('<Body />', () => {
  
  it('should render a div', () => {
    const wrapper = shallow(<Body />)
    expect(wrapper.find('div').length).toEqual(1)
  })
  
  it('should render its children', () => {
    const Stuff = () => <span />
    const wrapper = shallow(<Body><Stuff /></Body>)
    expect(wrapper.find(Stuff).length).toMatchSnapshot()
  })
  
})