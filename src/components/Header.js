import React from 'react'

import AppBar from 'material-ui/AppBar'
import Toggle from 'material-ui/Toggle'

const Header = ({onToggle}) =>
  <AppBar
    title="Random app"
    iconElementRight={
      <Toggle
        style={{margin: 16}}
        thumbSwitchedStyle={{backgroundColor: '#000'}}
        trackSwitchedStyle={{backgroundColor: '#fff',}}
        trackStyle={{backgroundColor: '#000',}}
        onToggle={(_, isInputChecked) => onToggle(isInputChecked)}
      />
    }
  />
export default Header