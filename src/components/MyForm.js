import React, { Component } from 'react'

const styles = {
  form: {marginTop: 50},
  input: {padding: 5, width: '5em'},
  button: {padding: 5},
}

const isValid = num => (num !== '' && num > -1 && num < 11)

class MyForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      value: '',
      isValid: false
    }
    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange () {
    const value = this.input.value

    this.setState( state => ({
      ...state,
      value,
      isValid: isValid(value)
    }))
  }

  onSubmit (e) {
    e.preventDefault()
    this.props.onSubmit(this.state.value)
  }

  render () {
    return (
      <form style={styles.form} onSubmit={this.onSubmit}>

        <input
          type='number'
          ref={ elm => { this.input = elm } }
          style={styles.input}
          onChange={this.onChange}
          autoFocus />

        <button
          style={styles.button}
          disabled={!this.state.isValid}>OK</button>

      </form>
    )
  }
}


export default MyForm
