import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'

const Decrement = ({onClick}) =>
  <RaisedButton onTouchTap={onClick}>-</RaisedButton>
  
export default Decrement