import React from 'react'

import Snackbar from 'material-ui/Snackbar'

const Comment = ({text}) =>
  <Snackbar
    open={true}
    message={text}
    autoHideDuration={1000}
  />
  
export default Comment