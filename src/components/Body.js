import React from 'react'

const style = {
  padding: 30,
  textAlign: 'center',
}

const Body = ({children}) =>
  <div style={style}>{children}</div>

export default Body