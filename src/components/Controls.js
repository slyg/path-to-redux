import React from 'react'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'

const Controls = ({onAdd}) =>
  <div style={{ paddingTop: 30, }}>
    <FloatingActionButton onTouchTap={onAdd}>
      <ContentAdd />
    </FloatingActionButton>
  </div>
  
export default Controls