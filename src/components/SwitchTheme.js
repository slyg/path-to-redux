import React from 'react'
import FlatButton from 'material-ui/FlatButton'

const SwitchTheme = ({onClick}) =>
  <FlatButton style={{color: '#fff', position: 'relative', top: '1.5vh'}} onTouchTap={onClick} label="Switch Theme" />
  
export default SwitchTheme