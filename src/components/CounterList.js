import React from 'react'

import Counter from './Counter'

const CounterList = ({counts, onIncrement, onDecrement, isDark}) =>
  <div>
    {counts.map((count, index) =>
      <div key={index} style={{display: 'inline-block', padding: 20}}>
        <Counter 
          value={count} 
          onIncrement={ () => { onIncrement(index) }} 
          onDecrement={ () => { onDecrement(index) }}
          isDark={isDark}
        />
      </div>
    )}
  </div>
  

export default CounterList