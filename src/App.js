import React, { Component } from 'react'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import HeaderContainer from './containers/HeaderContainer'
import CountersContainer from './containers/CountersContainer'
import ControlsContainer from './containers/ControlsContainer'
import FormContainer from './containers/FormContainer'

import Body from './components/Body'

import { Provider } from 'react-redux'
import injectTapEventPlugin from 'react-tap-event-plugin'

import store from './store'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider>
          <div>
            <HeaderContainer />
            <Body>
              <CountersContainer />
              <ControlsContainer />
              <FormContainer />
            </Body>
          </div>
        </MuiThemeProvider>
      </Provider>
    )
  }
}

injectTapEventPlugin()

export default App
