import { combineReducers } from 'redux'

import counters from './counters'
import theme from './theme'

/*
 * {
 *    counters: [Number]
 *    theme: String
 * }
 */

// stateReducer :: (state, action) => newState
export default combineReducers({
  counters,
  theme
})
