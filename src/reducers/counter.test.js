import counter from './counter'
import {
  INCREMENT,
  DECREMENT,
} from '../actionTypes'

const initialAction = { type: 'WHATEVER' }

describe('counter reducer', () => {

  it('should return 0 as default value', () => {
    const nextState = counter(undefined, initialAction)
    expect(nextState).toMatchSnapshot()
  })
  
  it('should increment on INCREMENT action', () => {
    const state0 = counter(undefined, initialAction)
    const state1 = counter(state0, { type: INCREMENT })
    const state2 = counter(state1, { type: INCREMENT })
    
    expect(state2).toEqual(2)
  })
  
  it('should not decrement above 1 on DECREMENT action', () => {
    const state0 = counter(undefined, initialAction) // 0
    const state1 = counter(state0, { type: INCREMENT }) // 1
    const state2 = counter(state1, { type: DECREMENT }) // 1
    
    expect(state2).toEqual(1)
  })
  
  it('should return the previous state on unmatched action types', () => {
    const state0 = counter(undefined, initialAction) // 0
    const state1 = counter(state0, { type: INCREMENT }) // 1
    const state2 = counter(state1, { type: 'HOHO' }) // 1
    
    expect(state2).toEqual(1)
  })
  

})