import { UPDATE_COMMENT, } from '../actionTypes'

const initialState = 'No comment'

// count :: (state, action) => newState
const comment = (state = initialState, action) => {
  
  switch (action.type) {
    
    case UPDATE_COMMENT:
      return action.value
      
    default:
      return state
      
  }
  
}

export default comment
