import {
  INCREMENT,
  DECREMENT,
  ADD,
  SET_NUMBER_OF_COUNTERS,
} from '../actionTypes'

import counter from './counter'

const initialState = [0]

// counts :: (state, action) => newState
const counts = (state = initialState, action) => {

  switch (action.type) {

    case INCREMENT:
    case DECREMENT:
      const { index } = action
      return state.map( (count, i) => {
        if (index === i) {
          return counter(count, action)
        }
        return count
      } )

    case ADD:
      const newCount = counter(undefined, { action: 'WHATEVER' })
      return [...state, newCount]

    case SET_NUMBER_OF_COUNTERS:
      const { num } = action
      return [...Array(num).keys()].map(() => counter(undefined, { action: 'WHATEVER' }))

    default:
      return state

  }

}

export default counts
