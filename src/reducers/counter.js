import {
  INCREMENT,
  DECREMENT,
} from '../actionTypes'

const initialState = 0

// count :: (state, action) => newState
const count = (state = initialState, action) => {
  
  switch (action.type) {
    
    case INCREMENT:
      return state + 1
      
    case DECREMENT:
      if (state > 1) { return state - 1 } else {
        return state
      }
      
    default:
      return state
      
  }
  
}

export default count
