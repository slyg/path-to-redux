import {
  SWITCH_THEME
} from '../actionTypes'

const initialState = 'light'

// count :: (state, action) => newState
const theme = (state = initialState, action) => {
  
  switch (action.type) {
    
    case SWITCH_THEME:
      return (state === 'light') ? 'dark' : 'light'
      
    default:
      return state
      
  }
  
}

export default theme
