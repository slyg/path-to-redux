import { INCREMENT, DECREMENT } from '../actionTypes'
import { updateComment } from '../actionCreators'

const mw = store => next => action => {

  next(action)
  
  const { count } = store.getState()
  
  switch (action.type) {
    
    case INCREMENT:
    case DECREMENT:
      fetch(`https://jsonplaceholder.typicode.com/comments/${count}`)
        .then(resp => resp.json())
        .then(comment => store.dispatch(updateComment(comment.name)))
      break;
      
    default:
    
  }
  
  
}

export default mw
