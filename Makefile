.IMG_NAME = redux/sample

/build/:
	$(MAKE) dependencies
	docker run --rm \
		-v $(PWD)/build:/build \
		-v $(PWD)/public:/public \
		-v $(PWD)/src:/src \
		$(.IMG_NAME) yarn build

build: /build/

dependencies: package.json yarn.lock
	docker build -t $(.IMG_NAME) .

tests: src/
	$(MAKE) dependencies
	docker run --rm -it \
		-v $(PWD)/src:/src \
		$(.IMG_NAME) ./node_modules/.bin/jest src/**/*.test.js

watch: src/
	$(MAKE) dependencies
	docker run --rm -it \
		-p 3000:3000 \
		-v $(PWD)/build:/build \
		-v $(PWD)/public:/public \
		-v $(PWD)/src:/src \
		$(.IMG_NAME) yarn start

.PHONY 				= build dependencies tests watch
.DEFAULT_GOAL = build
